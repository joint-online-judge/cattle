﻿import { IRoute } from 'umi';

const routes: Array<IRoute> = [
  {
    path: '/login',
    component: '@/pages/Login',
    wrappers: ['@/wrappers/revAuth'],
  },
  {
    path: '/oauth-register',
    component: '@/pages/OAuthRegister',
    wrappers: ['@/wrappers/revAuth'],
  },
  { path: '/logout', component: '@/pages/Logout' },

  // @chujie: this route tree should be separated.
  // Otherwise, 'domainUrl' cannot be matched in the layout component.
  {
    path: '/domain/:domainUrl',
    component: '@/layouts/index',
    wrappers: ['@/wrappers/Auth'],
    routes: [
      {
        path: '/domain/:domainUrl/create-problem-set',
        component: '@/pages/CreateProblemSet',
      },
      {
        path: '/domain/:domainUrl/create-problem',
        component: '@/pages/CreateProblem',
      },
      {
        path: '/domain/:domainUrl/join',
        component: '@/pages/JoinDomain',
      },

      {
        path: '/domain/:domainUrl/settings/:tab/:subTab?',
        component: '@/pages/DomainSettings',
        routes: [
          {
            path: '/domain/:domainUrl/settings/profile',
            component: '@/pages/DomainSettings/Profile',
            menuKey: 'profile',
          },
          {
            path: '/domain/:domainUrl/settings/invitation',
            component: '@/pages/DomainSettings/Invitation',
            menuKey: 'invitation',
          },
          {
            path: '/domain/:domainUrl/settings/member',
            component: '@/pages/DomainSettings/Member',
            menuKey: 'member',
          },
          {
            path: '/domain/:domainUrl/settings/permission/role',
            component: '@/pages/DomainSettings/Permission/Role',
            menuKey: 'permission',
          },
          {
            path: '/domain/:domainUrl/settings/permission/config',
            component: '@/pages/DomainSettings/Permission/Config',
            menuKey: 'permission',
          },
          {
            path: '/domain/:domainUrl/settings/permission',
            redirect: '/domain/:domainUrl/settings/permission/config',
          },
          { component: '@/pages/NotFound' },
        ],
      },
      {
        path: '/domain/:domainUrl/settings',
        redirect: '/domain/:domainUrl/settings/profile',
      },

      {
        path: '/domain/:domainUrl/problem-set/:problemSetId/scoreboard',
        component: '@/pages/ProblemSetDetail/Scoreboard',
      },
      {
        path: '/domain/:domainUrl/problem-set/:problemSetId/system-test',
        component: '@/pages/ProblemSetDetail/SystemTest',
      },
      {
        path: '/domain/:domainUrl/problem-set/:problemSetId/settings',
        component: '@/pages/ProblemSetDetail/Settings',
      },
      {
        path: '/domain/:domainUrl/problem-set/:problemSetId/p/:problemId',
        component: '@/pages/ProblemDetail',
      },
      {
        path: '/domain/:domainUrl/problem-set/:problemSetId/:tab?',
        component: '@/pages/ProblemSetDetail',
        wrappers: ['@/wrappers/Auth'],
        routes: [
          {
            path: '/domain/:domainUrl/problem-set/:problemSetId/edit',
            component: '@/pages/ProblemSetDetail',
          },
          {
            path: '/domain/:domainUrl/problem-set/:problemSetId',
            component: '@/pages/ProblemSetDetail',
          },
          { component: '@/pages/NotFound' },
        ],
      },
      {
        path: '/domain/:domainUrl/problem-set',
        component: '@/pages/ProblemSetList',
        wrappers: ['@/wrappers/Auth'],
      },

      {
        path: '/domain/:domainUrl/problem/:problemId/settings',
        component: '@/pages/ProblemConfig',
        wrappers: ['@/wrappers/Auth'],
      },
      {
        path: '/domain/:domainUrl/problem/:problemId/:tab?',
        component: '@/pages/ProblemDetail',
        wrappers: ['@/wrappers/Auth'],
      },
      {
        path: '/domain/:domainUrl/problem',
        component: '@/pages/ProblemList',
        wrappers: ['@/wrappers/Auth'],
      },
      {
        path: '/domain/:domainUrl',
        component: '@/pages/DomainHome',
        wrappers: ['@/wrappers/Auth'],
      },
      { component: '@/pages/NotFound' },
    ],
  },

  {
    path: '/',
    component: '@/layouts/index',
    wrappers: ['@/wrappers/Auth'],
    routes: [
      { exact: true, path: '/', component: '@/pages/DomainList' },
      {
        exact: true,
        path: '/settings/:tab',
        component: '@/pages/UserSettings',
        routes: [
          {
            path: '/settings/general',
            component: '@/pages/UserSettings/General',
            menuKey: 'general',
            i18nKey: 'settings.site.general',
          },
          {
            path: '/settings/account',
            component: '@/pages/UserSettings/Account',
            menuKey: 'account',
            i18nKey: 'settings.site.account',
          },
          {
            path: '/settings/domains',
            component: '@/pages/UserSettings/General',
            menuKey: 'domains',
            i18nKey: 'settings.site.domains',
          },
        ],
      },
      {
        exact: true,
        path: '/settings',
        redirect: '/settings/general',
      },
      { exact: true, path: '/domain', component: '@/pages/DomainList' },
      { exact: true, path: '/admin', redirect: '/admin/domain' },
      {
        path: '/user/:username',
        component: '@/pages/Profile',
      },

      {
        path: '/admin/:tab',
        component: '@/pages/SiteAdmin',
        access: 'isRoot',
        routes: [
          {
            path: '/admin/domain',
            component: '@/pages/SiteAdmin/CreateDomain',
            menuKey: 'domain',
            i18nKey: 'admin.menu.domain',
          },
        ],
      },

      {
        path: '/records',
        component: '@/pages/RecordList',
      },

      { component: '@/pages/NotFound' },
    ],
  },

  { component: '@/pages/NotFound' },
];

const recursiveInjectWrapper = (routes: Array<IRoute>, wrapper: string) => {
  for (const route of routes) {
    if (route.wrappers instanceof Array) {
      route.wrappers.push(wrapper);
    } else {
      route.wrappers = [wrapper];
    }

    if (route.routes instanceof Array)
      recursiveInjectWrapper(route.routes, wrapper);
  }
};

recursiveInjectWrapper(routes, '@/wrappers/Access');

export default routes;
