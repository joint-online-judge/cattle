// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'settingsForm': string;
  'settingsSideBar': string;
  'settingsTitle': string;
  'userInfoHeader': string;
}
export const cssExports: CssExports;
export default cssExports;
