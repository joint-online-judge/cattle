interface ProTablePagination {
    pageSize?: number;
    current?: number;
}

interface HorsePagination {
    offset?: number;
    limit?: number
}
