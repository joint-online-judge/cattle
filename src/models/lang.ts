import { useState, useCallback } from 'react';
import { getAllLocales, getLocale, setLocale } from 'umi';

/**
 * Global language model.
 * Encapsulation of umi locale plugin.
 */
export default function LangModel() {
  const [currentLang, setCurrentLang] = useState<string>(getLocale());
  const allLang = getAllLocales(); // no plural because of spell checking

  const switchLang = useCallback(
    (lang: string) => {
      setCurrentLang(lang);
      setLocale(lang, false);
    },
    [allLang],
  );

  return {
    allLang,
    currentLang,
    switchLang,
  };
}
