export default {
  // lower-case noun
  domain: '域',
  // 简单名词
  DOMAIN: '域',
  PROBLEM: '题目',
  CREATION: '创建',
  HOMEWORK: '作业',
  HOME: '首页',
  TITLE: '标题',
  ADMIN: '管理',
  PERMISSION: '权限',
  // 简单动词
  CREATE: '创建',
  ADD: '增加',
  VIEW: '查看',
  VISIT: '查看',
  UPDATE: '更新',
  DELETE: '删除',
  SUBMIT: '提交',
  MANAGE: '管理',
  join: '加入',
  // User
  'USER.PROFILE': '我的资料',
  'USER.LOGIN.JACCOUNT_LOG_IN': 'jAccount 登录',
  'USER.LOGIN.REDIRECTING': '正在跳转到登录界面...',
  'USER.LOG_OUT': '登出',
  // Domain
  'DOMAIN.DOMAINS': '我的域',
  'DOMAIN.SETTINGS': '设置',
  'DOMAIN.NEW_DOMAIN': '新建域',
  'DOMAIN.LEAVE': '退出',
  'DOMAIN.CREATE_A_NEW_DOMAIN': '创建域',
  'DOMAIN.CREATE.NAME': '名字',
  'DOMAIN.CREATE.URL': '域网址 (ID)',
  'DOMAIN.CREATE.GRAVATAR': 'Gravatar 邮箱',
  'DOMAIN.CREATE.BULLETIN': '公告',
  'domain.create.tag': '标签',
  'DOMAIN.HOME.ASSIGNMENTS': '作业',
  'DOMAIN.HOME.PROBLEMS': '题目',
  'DOMAIN.HOME.MEMBERS': '成员',
  'DOMAIN.HOME.SETTINGS': '设置',
  'domain.invitation.join': '加入域',
  'domain.invitation.code': '邀请码',
  // Problems
  'problem.problems': '题库',
  'PROBLEM.CREATE.TITLE': '创建问题',
  'PROBLEM.CREATE.FORM.CONTENT': '描述',
  'PROBLEM.CREATE.FORM.HIDDEN': '隐藏',
  'PROBLEM.CREATE.FORM.URL': '问题URL',
  'PROBLEM.LANGUAGES': '编程语言',
  'PROBLEM.SUBMIT': '提交',
  'PROBLEM.STATUS': '提交状态',
  'PROBLEM.PROBLEM_GROUP': '问题组',
  'PROBLEM.OWNER': '出题人',
  'PROBLEM.HOME': '查看题目',
  'PROBLEM.SUBMIT_CODE': '提交代码',
  'PROBLEM.EDIT': '编辑题面',
  'PROBLEM.SETTINGS': '评测配置',
  'PROBLEM.RECENT_RECORD': '最近提交记录',
  'PROBLEM.UPLOAD_FILE': '上传代码文件',
  'PROBLEM.UPLOAD_HELP_CLICK': '上传文件',
  'PROBLEM.UPLOAD_HELP_DRAG': '或者拖拽文件至此处',
  'PROBLEM.UPLOAD_FILE.MISSING': '请上传代码文件',
  'PROBLEM.MEMORY_KB': '峰值内存',
  'PROBLEM.TIME_MS': '总耗时',
  'PROBLEM.SUBMIT_AT': '递交时间',
  // ProblemSet
  'PROBLEM_SET.PROBLEM_SET': '问题集',
  'PROBLEM_SET.INTRODUCTION': '介绍',
  'PROBLEM_SET.CREATE.FORM.CONTENT': '描述',
  'PROBLEM_SET.CREATE.FORM.HIDDEN': '隐藏',
  'PROBLEM_SET.CREATE.TITLE': '创建问题集',
  'PROBLEM_SET.CREATE.FORM.SCOREBOARD_HIDDEN': '隐藏排行榜',
  'PROBLEM_SET.CREATE.FORM.UNLOCK_AT': '开始提交时间',
  'PROBLEM_SET.CREATE.FORM.DUE_AT': '结束时间',
  'PROBLEM_SET.CREATE.FORM.LOCK_AT': '截止提交时间',
  'PROBLEM_SET.CREATE.FORM.URL': '问题集URL',
  'problem_set.side_menu.detail': '问题集详情',
  'problem_set.side_menu.scoreboard': '成绩榜',
  'problem_set.side_menu.system_test': '评测与导出',
  'problem_set.side_menu.edit': '编辑信息',
  'problem_set.side_menu.settings': '问题配置',
  // Settings
  'SETTINGS.SETTINGS': '偏好设置',
  'SETTINGS.GENERAL_SETTINGS': '通用设置',
  'SETTINGS.ACCOUNT_SETTINGS': '账户设置',
  'SETTINGS.SECURITY_SETTINGS': '安全设置',
  'SETTINGS.I18N_LANG': '语言',
  'SETTINGS.TIMEZONE': '时区',
  'SETTINGS.SWITCH_LANG': '切换语言',
  'SETTINGS.UPDATE_SETTINGS': '更新设置',
  'SETTINGS.DISPLAY_SETTINGS': '显示设置',
  'SETTINGS.DOMAIN': '域设置',
  'SETTINGS.DOMAIN.PROFILE': '基本资料',
  'SETTINGS.DOMAIN.INVITATION': '成员邀请',
  'SETTINGS.DOMAIN.MEMBERS': '成员管理',
  'SETTINGS.DOMAIN.PERMISSION': '权限管理',
  'settings.domain.permission.config': '权限控制',
  'settings.domain.permission.role': '角色管理',
  // Admin
  'admin.menu.domain': '域管理',
  // Footer
  'FOOTER.ABOUT': '关于',
  'FOOTER.API': 'API',
  'FOOTER.DOCS': '文档',
  'FOOTER.ISSUE': 'Bug反馈',
  'FOOTER.CONTACT': '联系我们',
  // Languages
  'zh-CN': '简体中文',
  'en-US': 'English',
  // Form
  'form.input_placeholder': '请输入',
  'form.select_placeholder': '请选择',
  'form.domain_candidate_search.placeholder': '按用户名/邮箱/学号/真名搜索',
  'form.domain_role_select.placeholder': '请选择一个角色',
  'form.domain_role_select.error_placeholder': '加载选项失败',
  // Messages
  'msg.success.fetch': '获取成功',
  'msg.success.create': '创建成功',
  'msg.success.update': '更新成功',
  'msg.success.delete': '删除成功',
  'msg.success.fetch_item': '获取{data}成功',
  'msg.success.create_item': '创建{data}成功',
  'msg.success.update_item': '更新{data}成功',
  'msg.success.delete_item': '删除{data}成功',
  'msg.error.fetch': '获取失败',
  'msg.error.create': '创建失败',
  'msg.error.update': '更新失败',
  'msg.error.delete': '删除失败',
  'msg.error.fetch_item': '获取{data}失败',
  'msg.error.create_item': '创建{data}失败',
  'msg.error.update_item': '更新{data}失败',
  'msg.error.delete_item': '删除{data}失败',
  // Menu
  'menu.domain': '我的域',
  'menu.admin': '全站管理',
  'menu.domain.overview': '概要',
  'menu.domain_manage': '域管理',
  'menu.problem_list': '题库',
  'menu.problem_set': '问题集',
};
